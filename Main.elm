module ActivitiesHistories exposing (..)

import Time exposing (..)
import Date exposing (..)
import Dict exposing (..)
import Element exposing (..)
import Element.Input as Input
import MyStyles
import Html
import Element.Attributes exposing (..)
import Element.Events exposing (..)
import List.Extra as ListExtras


-- MODEL


type alias AppState =
    { activitiesHistories : ActivitiesHistories
    , editableActivity : ActivityEditorState
    , editablActivityType : String
    , timePeriod : TimePeriod
    }



-- activity editor


type alias ActivityEditorState =
    { timeStamp : Maybe String
    , activityName : Maybe String
    , activityDropDown : Input.SelectWith String ActivityEditorMsg
    , comment : String
    , workaroundCounter : Int
    , open : Bool
    }


type alias ActivitiesHistories =
    Dict String ActivityHistory


type alias ActivityHistory =
    { activities : Activities
    , currentGoal : Goal
    , goals : List Goal
    }


type alias Comment =
    String


type alias Activities =
    Dict Time Comment


type alias Activity =
    { activityName : String
    , when : Time
    , comment : String
    }



-- GOALS


type TimePeriod
    = Week
    | TwoWeeks
    | ThreeWeeks
    | FourWeeks
    | Month
    | TwoMonths



-- Goals are a linked list with at least one member. This is because an activity without a goal defeats the purpose of the application
-- The head of the list is the current goal. You can replace a previous goal with a new one by adding to the list.
-- the end date of a previous goal is simply the start of the new one :)
-- goals cannot be shorter than a single day and start on the day that they were created


type alias Goal =
    { startDay : Date
    , period : TimePeriod
    , nTimes : Int
    }



-- INIT


init : ( AppState, Cmd Msg )
init =
    ( AppState (initAcivitiesHistories) (initActivityEditor) "" Week, Cmd.none )


initAcivitiesHistories : ActivitiesHistories
initAcivitiesHistories =
    Dict.empty


initActivityEditor : ActivityEditorState
initActivityEditor =
    ActivityEditorState Nothing Nothing (Input.dropMenu Nothing SetActivity) "" 0 False



-- UPDATE
-- replaceGoal, addActivityType, removeActivityType, addActivity


type Msg
    = AddActivityType String Goal
    | ReplaceGoal String Goal
    | RemoveActivityType String
    | RemoveActivity String Time
    | ActivityEditorMsg ActivityEditorMsg
    | ActivityTypeEditorMsg ActivityTypeEditorMsg


type ActivityEditorMsg
    = StampTime String
    | SetActivity (Input.SelectMsg String)
    | SetComment String
    | Open
    | Close
    | Save


type ActivityTypeEditorMsg
    = Change String
    | SaveActivityType


updateActivityEditor : ActivityEditorMsg -> ActivityEditorState -> ( ActivityEditorState, Maybe Activity )
updateActivityEditor msg model =
    case msg of
        Open ->
            --might not need this one
            ( { model | open = True }, Nothing )

        Close ->
            ( { model | open = False }, Nothing )

        StampTime time ->
            ( { model | timeStamp = (Just time) }, Nothing )

        SetActivity activityName ->
            ( { model
                | activityDropDown = Input.updateSelection activityName model.activityDropDown
                , activityName = Input.selected model.activityDropDown
              }
            , Nothing
            )

        SetComment comment ->
            ( { model | comment = comment }, Nothing )

        Save ->
            case ( model.activityName, model.timeStamp ) of
                ( Just activityName, Just timeStamp ) ->
                    let
                        time =
                            parseTime timeStamp
                    in
                        case time of
                            Just time ->
                                --valid model? ==> add to list and reset the editor
                                ( { model
                                    | timeStamp = Nothing
                                    , activityName = Nothing
                                    , activityDropDown = Input.clear model.activityDropDown
                                    , comment = ""
                                    , workaroundCounter = model.workaroundCounter + 1
                                  }
                                , Just (Activity activityName time model.comment)
                                )

                            Nothing ->
                                ( model, Nothing )

                ( _, _ ) ->
                    --invalid model ==> dont
                    ( model, Nothing )


parseTime : String -> Maybe Time
parseTime timeString =
    case (Date.fromString timeString) of
        Ok date ->
            Just (Date.toTime date)

        Err error ->
            Nothing


updateActivityTypeEditor : ActivityTypeEditorMsg -> String -> ( String, Maybe String )
updateActivityTypeEditor msg model =
    case msg of
        Change activity ->
            ( activity, Nothing )

        SaveActivityType ->
            ( model, Just model )


update : Msg -> AppState -> ( AppState, Cmd Msg )
update msg model =
    case msg of
        AddActivityType activityName goal ->
            ( { model | activitiesHistories = (addActivityType activityName goal model.activitiesHistories) }, Cmd.none )

        ReplaceGoal activityName goal ->
            ( { model | activitiesHistories = (replaceGoal activityName goal model.activitiesHistories) }, Cmd.none )

        RemoveActivityType activityName ->
            ( { model | activitiesHistories = (removeActivityType activityName model.activitiesHistories) }, Cmd.none )

        RemoveActivity activityName when ->
            ( { model | activitiesHistories = (removeActivity activityName when model.activitiesHistories) }, Cmd.none )

        ActivityEditorMsg msg ->
            let
                ( newEditor, activity ) =
                    updateActivityEditor msg model.editableActivity
            in
                case activity of
                    Nothing ->
                        ( { model | editableActivity = newEditor }, Cmd.none )

                    Just activity ->
                        ( { model
                            | editableActivity = newEditor
                            , activitiesHistories = (addActivity activity model.activitiesHistories)
                          }
                        , Cmd.none
                        )

        ActivityTypeEditorMsg msg ->
            let
                ( newEditor, activityName ) =
                    (updateActivityTypeEditor msg model.editablActivityType)
            in
                case activityName of
                    Nothing ->
                        ( { model | editablActivityType = newEditor }, Cmd.none )

                    Just activityName ->
                        ( { model | activitiesHistories = (addActivityType activityName (Goal (Date.fromTime 12) TwoMonths 1) model.activitiesHistories) }, Cmd.none )


newCurrentGoalHelper : Goal -> ActivityHistory -> ActivityHistory
newCurrentGoalHelper goal history =
    { history | goals = history.currentGoal :: history.goals, currentGoal = goal }


addActivityType : String -> Goal -> ActivitiesHistories -> ActivitiesHistories
addActivityType activityName goal histories =
    if (Dict.member activityName histories) then
        histories
    else
        (Dict.insert activityName { activities = Dict.empty, goals = [], currentGoal = goal } histories)


removeActivityType : String -> ActivitiesHistories -> ActivitiesHistories
removeActivityType activityName histories =
    Dict.remove activityName histories


replaceGoal : String -> Goal -> ActivitiesHistories -> ActivitiesHistories
replaceGoal activityName goal histories =
    let
        history =
            Dict.get activityName histories
    in
        case history of
            Just item ->
                if (item.currentGoal.startDay == goal.startDay) then
                    (Dict.insert activityName { item | currentGoal = goal } histories)
                else
                    (Dict.insert activityName (newCurrentGoalHelper goal item) histories)

            Nothing ->
                histories


{-| This will add a new activity or overwrite an existing one if it already exists
-}
addActivity : Activity -> ActivitiesHistories -> ActivitiesHistories
addActivity activity histories =
    let
        history =
            Dict.get activity.activityName histories
    in
        case history of
            Just item ->
                (Dict.insert activity.activityName { item | activities = (Dict.insert activity.when activity.comment item.activities) } histories)

            Nothing ->
                histories


removeActivity : String -> Time -> ActivitiesHistories -> ActivitiesHistories
removeActivity activityName time histories =
    let
        history =
            Dict.get activityName histories
    in
        case history of
            Just item ->
                (Dict.insert activityName { item | activities = (Dict.remove time item.activities) } histories)

            Nothing ->
                histories



-- VIEWHELPERS


{-| This is not directly and HTML view but rather another way of looking at the model that will be easier for views to consume
-}
timeOrderedActivities : ActivitiesHistories -> List Activity
timeOrderedActivities histories =
    histories
        |> Dict.toList
        |> List.concatMap
            (\( activityName, activityHistory ) ->
                activityHistory.activities
                    |> Dict.toList
                    |> List.map (\( time, comment ) -> { activityName = activityName, when = time, comment = comment })
            )
        |> List.sortBy (\elem -> elem.when)


areSameDay : Activity -> Activity -> Bool
areSameDay a b =
    let
        aa =
            Date.fromTime a.when

        bb =
            Date.fromTime b.when
    in
        (Date.year aa) == (Date.year bb) && (Date.month aa) == (Date.month bb) && (Date.day aa) == (Date.day bb)


dayGroupedActivities : List Activity -> List (List Activity)
dayGroupedActivities activities =
    ListExtras.groupWhile areSameDay activities


activityStates : ActivitiesHistories -> List String
activityStates histories =
    Dict.keys histories



-- VIEW


view : AppState -> Html.Html Msg
view model =
    Element.viewport MyStyles.stylesheet <|
        column MyStyles.None
            [ id "hello", verticalSpread, height fill, width fill ]
            [ (viewAllActivities ((timeOrderedActivities >> dayGroupedActivities) model.activitiesHistories ))
            , row MyStyles.None
                [ spacing 20, center, width fill ]
                [ (viewActivityTypes (activityStates model.activitiesHistories))
                , Element.map ActivityTypeEditorMsg (viewActivityTypeEditor model.editablActivityType)
                , Element.map ActivityEditorMsg (viewActivityEditor (activityStates model.activitiesHistories) model.editableActivity)
                ]
            ]


viewActivityTypesAsSelect : List String -> ActivityEditorState -> Element.Element MyStyles.MyStyles () ActivityEditorMsg
viewActivityTypesAsSelect activityTypes model =
    Input.select MyStyles.None
        []
        { max = 5
        , options = [ Input.textKey (toString model.workaroundCounter) ]
        , with = model.activityDropDown
        , label = Input.labelLeft (text "Activity Type*: ")
        , menu =
            Input.menu MyStyles.None
                []
                (List.map (\typ -> Input.choice typ (text typ)) activityTypes)
        }


viewActivityTypes : List String -> Element.Element MyStyles.MyStyles () Msg
viewActivityTypes activityTypes =
    column MyStyles.None
        []
        ([ el MyStyles.Title [] (text "Activity Types:") ]
            ++ (List.map (\activity -> el MyStyles.Entry [] (text activity)) activityTypes)
        )


viewActivityTypeEditor : String -> Element.Element MyStyles.MyStyles () ActivityTypeEditorMsg
viewActivityTypeEditor activityName =
    column MyStyles.None
        [ spacing 10 ]
        [ el MyStyles.Title [] (text "New Activity Type:")
        , Input.text MyStyles.None
            []
            { label =
                Input.placeholder
                    { label = Input.labelLeft (el MyStyles.None [] (text "Type :"))
                    , text = "new type!"
                    }
            , onChange = Change
            , options = []
            , value = activityName
            }
        , button MyStyles.None [ onClick SaveActivityType ] (text "Save")
        ]


viewActivityEditor : List String -> ActivityEditorState -> Element.Element MyStyles.MyStyles () ActivityEditorMsg
viewActivityEditor activities model =
    column MyStyles.None
        [ spacing 10 ]
        [ el MyStyles.Title [] (text "Edit Activity:")
        , Input.text MyStyles.None
            []
            { label =
                Input.placeholder
                    { label = Input.labelLeft (el MyStyles.None [] (text "Time* :"))
                    , text = "time o' clock"
                    }
            , onChange = StampTime
            , options = [ Input.textKey (toString model.workaroundCounter) ]
            , value =
                (case model.timeStamp of
                    Nothing ->
                        ""

                    Just ts ->
                        ts
                )
            }
        , viewActivityTypesAsSelect activities model
        , Input.multiline MyStyles.None
            []
            { label =
                Input.placeholder
                    { label = Input.labelLeft (el MyStyles.None [] (text "Comment :"))
                    , text = "Your comment"
                    }
            , onChange = SetComment
            , value = model.comment
            , options = [ Input.textKey (toString model.workaroundCounter) ]
            }
        , button MyStyles.None [ onClick Save ] (text "Save")
        ]


viewAllActivities : List (List Activity) -> Element.Element MyStyles.MyStyles () msg
viewAllActivities activities =
    column MyStyles.None
        [ height fill ]
        [ h1 MyStyles.Title [] (text "Timeline:")
        , row MyStyles.None
            [ spacing 10, height fill, center ]
            ((List.map viewActivityGroup activities)
                ++ [ el MyStyles.TimelineDivider [ verticalCenter, height (percent 70), width (px 5) ] (text "") ]
            )
        ]


viewActivityGroup : List Activity -> Element.Element MyStyles.MyStyles () msg
viewActivityGroup activities =
    column MyStyles.TimelineEntry
        [ padding 10, height content, verticalCenter ]
        (List.map viewActivity activities)


viewActivity : Activity -> Element.Element MyStyles.MyStyles () msg
viewActivity model =
    let
        activity =
            model.activityName

        timeStamp =
            (toString model.when)
    in
        column MyStyles.TimelineEntry
            [ padding 10, height content, verticalCenter ]
            [ h1 MyStyles.None [] (text activity)
            , h2 MyStyles.None [] (text timeStamp)
            , text model.comment
            ]



-- SUBSCRIPTIONS


subscriptions : AppState -> Sub Msg
subscriptions model =
    Sub.none



-- PROGRAM


main : Program Never AppState Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
