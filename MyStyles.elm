module MyStyles exposing (..)

import Style exposing (..)
import Style.Color as Color
import Style.Border as Border
import Color exposing (..)
import Style.Font as Font


type MyStyles
    = Title
    | Entry
    | TimelineEntry
    | TimelineDivider
    | None


stylesheet : StyleSheet MyStyles variation
stylesheet =
    Style.styleSheet
        [ Style.style Title
            [ Color.text Color.darkGrey
            , Font.size 20 -- all units given as px
            , Font.typeface
                [ Font.font "Helvetica"
                , Font.font "Comic Sans"
                , Font.font "Papyrus"
                ]
            ]
        , Style.style Entry
            [ Font.size 16 -- all units given as px
            , Font.typeface
                [ Font.font "Helvetica"
                , Font.font "Comic Sans"
                , Font.font "Papyrus"
                ]
            ]
        , Style.style TimelineEntry
            [ Color.background (Color.rgba 100 100 100 0.9)
            , Border.rounded 12
            ]
        , Style.style TimelineDivider
            [ Color.background (Color.rgba 200 200 200 0.9)
            , Border.rounded 12
            ]
        , Style.style None []
        ]
